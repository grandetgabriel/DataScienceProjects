// AJAX request does not work on GitLab pages so this is a workaround

var data = [{
    "title": "/r/dataisbeautiful top 100",
    "language": "R",
    "desc": "Data from Reddit .json. Manipulation, exploration, visualization, fitted normal distribution.",
    "notebook_src": "./data/DataIsBeautiful.html",
    "img_src": "./media/thumbnails/data.jp2",
    "value": 1
},
{
    "title": "Pokemon data",
    "language": "R",
    "desc": "Fun and simple analysis of Pokemon data set. Parallel graphs with ggplot.",
    "notebook_src": "./data/pokemon.html",
    "img_src": "./media/thumbnails/pokemon.jpg",
    "value": 1
},
{
    "title": "House Prices: Advanced Regression Techniques",
    "language": "R",
    "desc": "Data set from Kaggle. Advanced visualizations, imputation of missing data (a lot of missing data) and machine learning.",
    "notebook_src": "./data/housePrices.html",
    "img_src": "./media/thumbnails/house.jpg",
    "value": 2
},
{
    "title": "Google Analytics Customer Revenue Prediction",
    "language": "R",
    "desc": "In this competition, you’re challenged to analyze a Google Merchandise Store (also known as GStore, where Google swag is sold) customer dataset to predict revenue per customer.",
    "notebook_src": "./data/revenue.html",
    "img_src": "./media/thumbnails/house.jpg",
    "value": 3
},
{
    "title": "Forecasting currency prices",
    "language": "R",
    "desc": "Forecasting is the process of making predictions of the future based on past and present data and most commonly by analysis of trends. A commonplace example might be estimation of some variable of interest at some specified future date. ",
    "notebook_src": "./data/forecasting.html",
    "img_src": "./media/thumbnails/house.jpg",
    "value": 3
}
]

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

$(document).ready(function () {
    createCards();
});

function createCards() {
    console.log("List generator ready");

    var listItemTemplate = $("#list-item-template").html();

    var parentLists = [$("#project-list"), $("#all-project-list")];
    // var parentDeck = $("#card-deck")
    data.sort(dynamicSort("-value"));
    // console.log(data);

    // console.log(parentLists);

    // console.log(parentLists[0].length);

    // parentLists is a list of parent lists. If list exists its length is != 0
    // so we are only filling the list that exists
    
    $.each(parentLists, function (l, list) {
        if (parentLists[l].length != 0) {
            $.each(data, function (i, item) {
                // we don't want items with value less then 1
                // and we do not want more then 4 items on the main site
                // single & for reasons
                if (l == 0 & (item.value < 1 || i > 3)) { 
                    return;
                }
                parentLists[l].append(Mustache.render(listItemTemplate, item));
                console.log(i);
                
            });
        }
    });

    // AJAX request code
    // $.ajax({
    //     url: "./../data/text.json",
    //     dataType: "json",
    //     success: function (data) {
    //         $.each(data, function (i, item) {
    //             parentList.append(Mustache.render(listItemTemplate, item));
    //         })
    //     },
    //     error: function (data) {
    //         console.error("error");
    //     }
    // });
}