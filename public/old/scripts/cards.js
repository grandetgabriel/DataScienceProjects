// AJAX request does not work on GitLab pages so this is a workaround

var data = [{
        "title": "Iris",
        "language": "R",
        "desc": "Exploration on Iris data set, learning basic of data visualization and machine learning with Keras.",
        "notebook_src": "./data/iris.html",
        "img_src": "./media/thumbnails/iris.jp2",
        "value": 0
    },
    {
        "title": "Titanic",
        "language": "R",
        "desc": "In depth data exploration and machine learning. Understanding data, visualizations, transformations with tidyverse and Keras.",
        "notebook_src": "./data/titanic.html",
        "img_src": "./media/thumbnails/titanic.jp2",
        "value": 1
    },
    {
        "title": "/r/dataisbeautiful top 100",
        "language": "R",
        "desc": "Data from Reddit .json. Manipulation, exploration, visualization, fitted normal distribution.",
        "notebook_src": "./data/DataIsBeautiful.html",
        "img_src": "./media/thumbnails/data.jp2",
        "value": 1
    },
    {
        "title": "Pokemon data",
        "language": "R",
        "desc": "Fun and simple analysis of Pokemon data set. Parallel graphs with ggplot.",
        "notebook_src": "./data/pokemon.html",
        "img_src": "./media/thumbnails/pokemon.jpg",
        "value": 1
    },
    {
        "title": "House Prices: Advanced Regression Techniques",
        "language": "R",
        "desc": "Data set from Kaggle. Advanced visualizations, imputation of missing data (a lot of missing data) and machine learning.",
        "notebook_src": "./data/housePrices.html",
        "img_src": "./media/thumbnails/house.jpg",
        "value": 2
    }
]

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

$(document).ready(function () {
    createCards();
});

function createCards() {
    console.log("Card generator ready");

    var cardTemplate = $("#card-template").html();

    var parentRow = $("#projects-row");
    // var parentDeck = $("#card-deck")
    data.sort(dynamicSort("-value"));
    console.log(data);


    $.each(data, function (i, item) {
        parentRow.append(Mustache.render(cardTemplate, item));
    });

    // AJAX request code
    // $.ajax({
    //     url: "./../data/text.json",
    //     dataType: "json",
    //     success: function (data) {
    //         $.each(data, function (i, item) {
    //             parentRow.append(Mustache.render(cardTemplate, item));
    //         })
    //     },
    //     error: function (data) {
    //         console.error("error");
    //     }
    // });
}

setTimeout(function setCardsHeights() {
    var cardTexts = $(".card-text");
    var maxHeightText = $(cardTexts[0]).height();
    var cardTitles = $(".card-title");
    var maxHeightTitle = $(cardTitles[0]).height();
    $.each(cardTexts, function (i, item) {
        if (maxHeightText <= $(item).height()) {
            maxHeightText = $(item).height();
        }
    })
    $.each(cardTitles, function (i, item) {
        if (maxHeightTitle <= $(item).height()) {
            maxHeightTitle = $(item).height();
        }
    })

    $(cardTexts).height(maxHeight);
}, 700);