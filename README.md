# Welcome to my data science repo
This is the place where I keep all my data science projects. Each folder (except DataCampNotebooks) containt .Rmd file and a compiled .html file.  
Data is no longer stored in this repo. Every notebook will have link to the data set.  

### Main technologies used for Data Science
- R
- ggplot
- dplyr
- keras (learning)

## Projects website
Link to the landing page: https://tomasz.cichy98.gitlab.io/DataScienceProjects/  
Project list items are generated from a template. JSON request is not possible on GitLab Pages that are currently stored in the card generator script.  

## Timeline
1. <b>DataCamp Notebooks</b>
1. <b>Iris Data</b>
1. <b>MNIST Keras</b>
1. <b>Titanic</b>
1. <b>DataIsBeautiful</b>
1. <b>Pokemon</b>
1. <b>HousePrices</b>
1. <s>Python project...</s>
1. Google Analytics Customer Revenue Prediction
1. Forecasting currency rates
1. Song-book project (parallel)
1. Finish TGS
  
<b>Bold</b> means completed.

Bash script to copy notebook files from project dirs to /public/data <s>will be created soon.</s> is done.  
Connection not secure error will be fixed soon.
